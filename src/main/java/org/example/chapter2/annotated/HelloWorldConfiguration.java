package org.example.chapter2.annotated;

import org.example.chapter2.decoupled.HelloWorldMessageProvider;
import org.example.chapter2.decoupled.MessageProvider;
import org.example.chapter2.decoupled.MessageRenderer;
import org.example.chapter2.decoupled.StandardOutMessageRenderer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HelloWorldConfiguration {

    @Bean
    public MessageProvider provider() {
        return new HelloWorldMessageProvider();
    }

    @Bean
    public MessageRenderer renderer() {
        MessageRenderer renderer = new StandardOutMessageRenderer();
        renderer.setMessageProvider(provider());
        return renderer;
    }
}
