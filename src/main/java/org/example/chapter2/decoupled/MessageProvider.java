package org.example.chapter2.decoupled;

public interface MessageProvider {
    String getMessage();
}
