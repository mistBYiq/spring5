package org.example.chapter3.xml;

import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

//@Component("injectSimpleConfig")
@Configuration("injectSimpleConfig")
public class InjectSimpleConfig {

    private String name = "Mayer";
    private int age = 39;
    private float height = 1.91f;
    private boolean programmer = true;
    private Long ageInSeconds = 1_241_401_112L;

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public float getHeight() {
        return height;
    }

    public boolean isProgrammer() {
        return programmer;
    }

    public Long getAgeInSeconds() {
        return ageInSeconds;
    }
}
