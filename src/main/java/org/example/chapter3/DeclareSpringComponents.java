package org.example.chapter3;

import org.example.chapter3.annotation.MessageProvider;
import org.example.chapter3.annotation.MessageRenderer;
import org.springframework.context.support.GenericXmlApplicationContext;

public class DeclareSpringComponents {
    public static void main(String[] args) {
        GenericXmlApplicationContext context = new GenericXmlApplicationContext();
        context.load("classpath:spring/app-context-constructor.xml");
        context.refresh();
//        MessageRenderer messageRenderer = context.getBean("renderer", MessageRenderer.class);
//        messageRenderer.render();
//        context.close();
        MessageProvider messageProvider = context.getBean("provider", MessageProvider.class);
        System.out.println(messageProvider.getMessage());
    }
}
