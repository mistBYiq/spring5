package org.example.chapter3.ex;

public interface NewsletterSender {

    void setSmtpServer(String smtpServer);
    String getSmtpServer();

    void setFromAddress(String fromAddress);
    String getFromAddress();

    void send();
}
