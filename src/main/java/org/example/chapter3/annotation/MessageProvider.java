package org.example.chapter3.annotation;

public interface MessageProvider {
    String getMessage();
}
